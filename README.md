# Build a Docker image

This guide describes how to build and prepare a Docker container image that you can deploy in a Kubernetes cluster. As an example, we will build an image named `ExampleApp` listening on port 8800 for requests.

1. Download and install [Docker Desktop](https://www.docker.com/). 

2. Create directories for your project as shown below:

   ```bash
   mkdir quickstart_docker
   
   mkdir quickstart_docker/application
   
   mkdir quickstart_docker/docker
   
   mkdir quickstart_docker/docker/application
   ```

   **Note:** In the Windows Command Prompt, a backslash `\` is used as the directory path separator.

   `quickstart_docker`  will be the main directory of your project, so you should change the current working directory as follows:

   ```
   Cd quickstart_docker
   ```

3. Create a file with the code of your application named `application.py` in the `quickstart_docker/application` directory with the following contents:

   ```python
   import http.server
   import socketserver
   
   PORT = 8800
   
   Handler = http.server.SimpleHTTPRequestHandler
   
   httpd = socketserver.TCPServer(("", PORT), Handler)
   
   print("serving at port", PORT)
   httpd.serve_forever()
   ```

   **Note:** Use your actual file(s) with the application code when working on a real-world project.

4. Optionally, download a Docker image for your application from a container registry to have it on your local machine. For example, to download the Python image from Docker Hub, run the following command:

   ```bash
   Docker pull python:3.6
   ```

   Where `'3.6'` is a version of the image. If it is not specified, the latest version will be downloaded.

5. Create a file named `Dockerfile` in the `quickstart_docker/docker/application` directory with the following contents:

   ```
   # Pull a base image from the Docker Hub registry
   FROM python:3.5
   
   # Set the working directory to /app
   WORKDIR /app
   
   # Copy the 'application' directory contents into the container at /app
   COPY ./application /app
   
   # Make port 8800 available to the world outside this container
   EXPOSE 8800
   
   # Execute the 'python /app/application.py' command when the container launches
   CMD ["python", "/app/application.py"]
   ```

6. Run the `docker build` command to build a Docker image for your application:

   ```bash
   docker build . -f docker/application/Dockerfile -t exampleapp
   ```

   Where:

   - `'.'` is the build context which is the current directory
   - `'-f'` is a flag used to specify the path to the Dockerfile
   - `'-t'` is a flag used to specify a name for your Docker image. You can also add a tag after the specified name, e.g., `'exampleapp:1.3'`

7. View the created images using the `docker images` command:

   ```
   docker images
   ```

   After you run the command, the data in the following format should be displayed:

   ```
   REPOSITORY    TAG    IMAGE ID   CREATED     SIZE
   
   exampleapp   latest   83wse0edc28a  2 seconds ago 153MB
   
   python      3.6    05sob8636w3f  6 weeks ago  153MB
   ```

8. Push the created image to a registry. 

   For example, to push the image to Docker Hub:

   1. Sign up and log in to [Docker hub](https://hub.docker.com/) and create a repository.

   2. Run the `docker push` command as shown below:

      ```
      docker push <username>/<repository-name>:<tag>
      ```

 